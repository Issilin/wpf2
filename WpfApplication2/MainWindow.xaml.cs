﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;

namespace WpfApplication2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport(@"ConsoleApplication5.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        //public static extern void slnq(ref IntPtr result, [MarshalAs(UnmanagedType.LPStr)]string n, int p, int alpha);
        public static extern int slnq(ref IntPtr result, [MarshalAs(UnmanagedType.LPStr)]string alpha);

        [DllImport(@"ConsoleApplication5.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern int slnq2(ref IntPtr result, [MarshalAs(UnmanagedType.LPStr)]string n);

        public MainWindow()
        {
            InitializeComponent();
        }

        private void CalculateFirstCase(object sender, RoutedEventArgs e)
        {
            
            IntPtr ptr_start = IntPtr.Zero;

            String result = "";

            int numberOfAnswer = slnq(ref ptr_start, FactAlpha.Text);

            String[] results = readResult(numberOfAnswer, ptr_start);

            for (int i = 0; i < numberOfAnswer; i++)
            {
                result += String.Format("PSL{0}({1}^{2}), ", FactN.Text, FactP.Text, results[i]);
            }

            try
            {
                Result.Text = result;
            }
            catch (Exception exc)
            {
                ;
            }

        }

        private void CalculateSecondCase(object sender, RoutedEventArgs e)
        {
            String result = "";

            IntPtr ptr_start = IntPtr.Zero;
            
            int numberOfAnswer = slnq2(ref ptr_start, MaskN.Text);
            
            String[] results = readResult(numberOfAnswer, ptr_start);

            for (int i = 0; i < numberOfAnswer; i++)
            {
                result += results[i] + ", ";
            }

            try
            {
                MaskResult.Text = result;
            }
            catch (Exception exc)
            {
                ;
            }
        }

        private String[] readResult(int countResults, IntPtr ptr_start) 
        {
            IntPtr[] ptr_strings = new IntPtr[countResults];
            Marshal.Copy(ptr_start, ptr_strings, 0, countResults);
            String[] strings = new string[countResults];

            for (int i = 0; i < countResults; i++)
            {
                strings[i] = Marshal.PtrToStringAnsi(ptr_strings[i]);
                
            }

            return strings;
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
